import {CUSTOMER_BY_ID_FAIL, CUSTOMER_BY_ID_SUCCESS} from "../slices/customerFindBySlice";

import {findById, getAll} from "../../services/CustomerRestService";


const getCustomerById = (id) => async (dispatch) => {
    try {
        const data = findById(id).then(response => {
            dispatch(CUSTOMER_BY_ID_SUCCESS(response.data));
            console.log(response.data);
        });
    }
    catch (error) {
        dispatch(CUSTOMER_BY_ID_FAIL(error))
    }
}

export default getCustomerById;