import {CUSTOMER_LIST_FAIL, CUSTOMER_LIST_SUCCESS} from "../slices/CustomerListSlice";

import {getAll} from "../../services/CustomerRestService";


const customerList = () => async (dispatch) => {
    try {
        const data = getAll().then(response => {
            dispatch(CUSTOMER_LIST_SUCCESS(response.data));
            console.log(response.data);
        });

    } catch (error) {
        dispatch(CUSTOMER_LIST_FAIL(error));
    }
};
export default customerList;
