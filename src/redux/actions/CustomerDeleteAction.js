import {CUSTOMER_DELETE_FAIL, CUSTOMER_DELETE_SUCCESS} from "../slices/CustomerDeleteSlice";

import {deleteCustomer} from "../../services/CustomerRestService";

//Use Async await rather than then
const customerDelete = (customer) => async (dispatch) => {
    console.log(customer.id)
    try {
        deleteCustomer(customer.id).then(res => dispatch(CUSTOMER_DELETE_SUCCESS(res.status)));
    } catch (error) {
        dispatch(CUSTOMER_DELETE_FAIL(error)); //Or Error
    }
};


export default customerDelete;