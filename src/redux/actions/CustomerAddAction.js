import {CUSTOMER_ADD_FAIL, CUSTOMER_ADD_SUCCESS} from "../slices/CustomerAddSlice"

import {postNewCustomer} from "../../services/CustomerRestService";

//Use Async await rather than then
const customerAdd = (customer) => async (dispatch) => {
    try {
        postNewCustomer(customer).then(res => dispatch(CUSTOMER_ADD_SUCCESS(res.status)));
    } catch (error) {
        dispatch(CUSTOMER_ADD_FAIL(error)); //Or Error
    }
};


export  default  customerAdd;