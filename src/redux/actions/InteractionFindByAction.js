import {INTERACTION_BY_ID_FAIL, INTERACTION_BY_ID_SUCCESS} from "../slices/InteractionFindBySlice";

import {findInteractionById} from "../../services/InteractionRestService";

const getInteractionsByClientId = (clientId) => async (dispatch) => {
    try {
        console.log("Action " + clientId)
        const data = findInteractionById(clientId).then(res => dispatch(INTERACTION_BY_ID_SUCCESS(res.data)));

    }
    catch (error) {
        dispatch(INTERACTION_BY_ID_FAIL(error))
    }
}

export default getInteractionsByClientId;