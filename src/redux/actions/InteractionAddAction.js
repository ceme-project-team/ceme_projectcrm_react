import{INTERACTION_ADD_SUCCESS,INTERACTIONS_ADD_FAIL} from "../slices/InteractionAddSlice";
import {postNewInteraction} from "../../services/InteractionRestService";

//Use Async await rather than then
const interactionAdd = (interaction) => async (dispatch) => {
    try {
        postNewInteraction(interaction).then(res => dispatch(INTERACTION_ADD_SUCCESS(res.status)));
    } catch (error) {
        dispatch(INTERACTIONS_ADD_FAIL(error)); //Or Error
    }
};


export  default  interactionAdd;