import {CUSTOMER_UPDATE_FAIL, CUSTOMER_UPDATE_SUCCESS} from "../slices/CustomerUpdateSlice";

import {updateCustomer} from "../../services/CustomerRestService";

const customerUpdate = (customer) => async (dispatch) => {
    try {
        updateCustomer(customer).then(res => dispatch(CUSTOMER_UPDATE_SUCCESS(res.status)));
    } catch (error) {
        dispatch(CUSTOMER_UPDATE_FAIL(error)); //Or Error
    }
};


export  default  customerUpdate;