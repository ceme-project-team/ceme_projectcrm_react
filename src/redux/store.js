import { configureStore } from '@reduxjs/toolkit';
import customerListSlice from "./slices/CustomerListSlice";
import customerAddSlice from "./slices/CustomerAddSlice";
import customerFindBySlice from "./slices/customerFindBySlice";
import customerDeleteSlice from "./slices/CustomerDeleteSlice";
import customerUpdateSlice from "./slices/CustomerUpdateSlice";
import interactionFindBySlice from "./slices/InteractionFindBySlice";

export default configureStore({
    reducer: {
        CustomerListReducer: customerListSlice,
        CustomerAddReducer: customerAddSlice,
        CustomerFindByIdReducer: customerFindBySlice,
        CustomerDeleteReducer: customerDeleteSlice,
        CustomerUpdateReducer: customerUpdateSlice,
        InteractionFindByIdReducer: interactionFindBySlice,
    },
});

