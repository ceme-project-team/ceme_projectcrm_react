import { createSlice } from '@reduxjs/toolkit';

export const customerDeleteSlice = createSlice({
    name: 'CustomerDeleteReducer',
    initialState: {
        customer: {},
        error: '',
        submitted: false,
    },
    reducers: {
        CUSTOMER_DELETE_SUCCESS: (state, action) => {
            state.submitted = true;
            state.customer = action.payload;
        },
        CUSTOMER_DELETE_FAIL: (state, action) => {
            state.submitted = true;
            state.error = action.payload;
        },
    },
});

export const {

    CUSTOMER_DELETE_SUCCESS,
    CUSTOMER_DELETE_FAIL,
} = customerDeleteSlice.actions;

export default customerDeleteSlice.reducer;
