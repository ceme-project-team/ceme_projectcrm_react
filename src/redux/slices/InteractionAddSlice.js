import { createSlice } from '@reduxjs/toolkit';

export const InteractionAddSlice = createSlice({
    name: 'InteractionAddReducer',
    initialState: {
        interaction: {},
        error: '',
        submitted: false
    },
    reducers:
        {
            INTERACTION_ADD_SUCCESS: (state, action) => {
                state.submitted = true;
                state.interaction = action.payload;
            },
            INTERACTIONS_ADD_FAIL: (state, action) => {
                state.submitted = true;
                state.interaction = action.payload;
            },
        },
});
export const {
    INTERACTION_ADD_SUCCESS,
    INTERACTIONS_ADD_FAIL
} = InteractionAddSlice.actions;

export default InteractionAddSlice.reducer;
