import {createSlice} from '@reduxjs/toolkit';

export const customerFindBySlice = createSlice({
    name: 'CustomerFindByIdReducer',
    initialState: {
        customer: {},
        error: '',
    },
    reducers: {
        CUSTOMER_BY_ID_SUCCESS: (state, action) => {
            state.pending = true;
            state.customer = action.payload;
        },
        CUSTOMER_BY_ID_FAIL: (state, action) => {
            state.pending = false;
            state.error = action.payload;
        },
    },
});

export const {

    CUSTOMER_BY_ID_SUCCESS,
    CUSTOMER_BY_ID_FAIL,
} = customerFindBySlice.actions

export default customerFindBySlice.reducer;