import {createSlice} from '@reduxjs/toolkit';

export const interactionFindBySlice = createSlice({
    name: 'InteractionFindByIdReducer',
    initialState: {
        interactions:  [],
        error: '',
    },
    reducers: {
        INTERACTION_BY_ID_SUCCESS: (state, action) => {
            state.submitted = true;
            state.interactions = action.payload;
        },
        INTERACTION_BY_ID_FAIL: (state, action) => {
            state.submitted = true;
            state.error = action.payload;
        },
    },
});

export const {

    INTERACTION_BY_ID_SUCCESS,
    INTERACTION_BY_ID_FAIL,
} = interactionFindBySlice.actions

export default interactionFindBySlice.reducer;