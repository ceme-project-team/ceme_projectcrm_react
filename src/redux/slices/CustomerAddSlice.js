import { createSlice } from '@reduxjs/toolkit';

export const customerAddSlice = createSlice({
    name: 'CustomerAddReducer',
    initialState: {
        customer: {},
        error: '',
        submitted: false,
    },
    reducers: {
        CUSTOMER_ADD_SUCCESS: (state, action) => {
            state.submitted = true;
            state.customer = action.payload;
        },
        CUSTOMER_ADD_FAIL: (state, action) => {
            state.submitted = true;
            state.error = action.payload;
        },
    },
});

export const {

    CUSTOMER_ADD_SUCCESS,
    CUSTOMER_ADD_FAIL,
} = customerAddSlice.actions;

export default customerAddSlice.reducer;
