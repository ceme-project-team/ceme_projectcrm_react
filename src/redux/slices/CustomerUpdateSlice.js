import {createSlice} from "@reduxjs/toolkit";

export  const customerUpdateSlice = createSlice({
    name: 'CustomerUpdateReducer',
    initialState: {
        customer: {},
        error: '',
        submitted: false,
    },
    reducers: {
        CUSTOMER_UPDATE_SUCCESS: (state, action) => {
            state.submitted = true;
            state.customer = action.payload;
        },
        CUSTOMER_UPDATE_FAIL: (state, action) => {
            state.submitted = true;
            state.error = action.payload;
        },
    },
});

export const {

    CUSTOMER_UPDATE_SUCCESS,
    CUSTOMER_UPDATE_FAIL,
} = customerUpdateSlice.actions;

export default customerUpdateSlice.reducer;
