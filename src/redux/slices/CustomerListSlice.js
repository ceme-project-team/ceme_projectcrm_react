import { createSlice } from '@reduxjs/toolkit';

export const CustomerListSlice = createSlice({
    name: 'CustomerListReducer',
    initialState: {
        pending: false,
        customers: [],
        error: '',
},
    reducers:
{
    CUSTOMER_LIST_SUCCESS: (state, action) => {
        state.pending = true;
        state.customers = action.payload;
    },
    CUSTOMER_LIST_FAIL:  (state, action) => {
        state.pending = false;
        state.error = action.payload;
    },
},
});

export const {
    CUSTOMER_LIST_SUCCESS,
    CUSTOMER_LIST_FAIL

} = CustomerListSlice.actions;

    export default CustomerListSlice.reducer;