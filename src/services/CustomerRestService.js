import http from "../http-common";

export const getAll = () => {
    return http.get("/all");
}

export const postNewCustomer = (object) => {
    return http.post('/postCustomer', object);
}

export const findById = (id) =>{
    return http.get(`/findById/${id}`)
}

export const deleteCustomer = (id) => {
    return http.delete(`/deleteCustomer/${id}`)
}

export const generateId =() =>{
    return http.get("/generateId");
}

export const updateCustomer = (object) => {
    return http.post('/postUpdate', object);
}

export const getStatus = () => {
    return http.get("/status");
}