import http from "../http-common";

export const postNewInteraction = (object) => {
    return http.post('/postActivity', object);
}

export const findInteractionById = (clientId) =>{
    return http.get(`/findByClientId/${clientId}`)
}
