import female from "../images/female.png"
import male from "../images/male.png"
import nonBin from "../images/non_bin.png"

export const getMaxID = (props) => {
    //max id in db + 1 for ID, auto populates so no conflicts in db
    const {customers} = props;

    let idArray = [0];
    let maxID = 0;
    let newID = 0;
    customers && customers.map((item, index) => (idArray[index] = item.id));
    maxID = idArray.reduce((function (a, b) {
        return Math.max(a, b)
    }))
    newID = maxID + 1;
    return newID;
}

export const picture = (gender) =>{

    let path = "";
    if (gender) {
        switch (gender.toString().toLowerCase()) {
            case "male":
                path = male;
                break;
            case "female":
                path = female;
                break;
            default:
                path = nonBin;
                break;
        }
        return path;
    }
    return nonBin;
}

export const nullOrEmpty = (validationItem,dataType)=>{
    let TrueFalse = true;

    switch (dataType.toString().toLowerCase()) {

        case "string":
            if (validationItem.trim() === "" || validationItem.trim().length() < 1){
                TrueFalse=false;
            }
            break;

        case "number":
           if (validationItem === 0 || validationItem.toString().trim().length <1
               || Number.isNaN((parseInt(validationItem))) || validationItem < 0)
           {
               TrueFalse = false;
           }
            break;

        default:break;
    }

    return TrueFalse; //return false if empty
}