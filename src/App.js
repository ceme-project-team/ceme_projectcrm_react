import React from 'react';
import './App.css';
import {BrowserRouter} from "react-router-dom";
import NavigationBar from "./components/Navigation";
import Router from "./components/Router";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <NavigationBar/>
        <Router/>
      </BrowserRouter>
    </div>
  );
}

export default App;
