import CustomerDetailForm from "./customerForm/customerForm";
import React from "react";

function UpdateCustomer(props) {

    return (
        <div>
            <h4>Update Customer</h4>
            < br/>
            <CustomerDetailForm customer={props.location.state}/>
        </div>
    );
}
export default UpdateCustomer;