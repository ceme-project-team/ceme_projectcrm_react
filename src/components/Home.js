import React from "react";
import '../styles/styles.css'
import background from '../images/background.jpg'
import cathy from "../images/cc.jpg"
import aziz from "../images/ak.jpg"
import natalia from "../images/np.jpg"
import mark from "../images/mc.jpg"

function Home(){

    return(
        <main>
            <header className="masthead" style={{backgroundImage: `url(${background})`,
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center center',
                backgroundAttachment: "scroll",
                backgroundSize: 'cover'}}>
                <div className="container">
                    <div className="masthead-subheading">Welcome To Our Customer Site!</div>
                    <div className="masthead-heading text-uppercase">It's Nice To Meet You</div>
                    <a className="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#features">Tell Me More</a>
                </div>
            </header>
            <section className="page-section" id="features">
                <div className="container">
                    <div className="text-center">
                        <h2 className="section-heading text-uppercase">Please see below for our features</h2>
                        <h3 className="section-subheading text-muted">Customers and their interactions</h3>
                    </div>
                    <div className="row text-center">
                    <div className="col-md-4">
                        <span className="fa-stack fa-4x">
                            <i className="fas fa-circle fa-stack-2x text-primary"></i>
                            <i className="fas fa-lock fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 className="my-3">Java API</h4>
                        <p className="text-muted">We provide a page that gives you the status of the back end REST API built using Java Spring data JPA.</p>
                    </div>

                        <div className="col-md-4">
                        <span className="fa-stack fa-4x">
                            <i className="fas fa-circle fa-stack-2x text-primary"></i>
                            <i className="fas fa-shopping-cart fa-stack-1x fa-inverse"></i>
                        </span>
                            <h4 className="my-3">Customers</h4>
                            <p className="text-muted"> On our site you can add, update and delete a customer,
                                you can also view a full list of all the customers using bootstrap cards.</p>
                        </div>
                        <div className="col-md-4">
                        <span className="fa-stack fa-4x">
                            <i className="fas fa-circle fa-stack-2x text-primary"></i>
                            <i className="fas fa-lock fa-stack-1x fa-inverse"></i>
                        </span>
                            <h4 className="my-3">Customer Interactions</h4>
                            <p className="text-muted">We also provide a feature to store customer interactions,
                            the back end Java API is fully functional for add, delete, view by customer, view by date range,
                            we provide a front end page for add interactions, and we have a view interaction by customer.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section className="page-section bg-light" id="team">
                <div className="container">
                    <div className="text-center">
                        <h2 className="section-heading text-uppercase">Our Amazing Team</h2>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="team-member">
                                <img className="mx-auto rounded-circle" src={aziz} alt=""/>
                                <h4>Aziz Kabir</h4>
                                <p></p>
                            </div>
                        </div>
                        <div className="col">
                            <div className="team-member">
                                <img className="mx-auto rounded-circle" src={cathy} alt=""/>
                                <h4>Cathy Cowan</h4>
                                <p></p>
                            </div>
                        </div>
                        </div>
                        <div className="row">
                            <div className="col">
                            <div className="team-member">
                                <img className="mx-auto rounded-circle" src={mark} alt=""/>
                                <h4>Mark Collins</h4>
                                <p></p>
                            </div>
                        </div>
                        <div className="col">
                            <div className="team-member">
                                <img className="mx-auto rounded-circle" src={natalia} alt=""/>
                                <h4>Natalia Podkarczemna</h4>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer className="footer py-4">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-4 text-lg-left">Copyright © Team 5 CEME 2020</div>
                        <div className="col-lg-4 my-3 my-lg-0"> </div>
                        <div className="col-lg-4 text-lg-right">
                            <a className="mr-3" href="#!">Privacy Policy</a>
                            <a href="#!">Terms of Use</a>
                        </div>
                    </div>
                </div>
            </footer>
        </main>
        )
}

export default Home;