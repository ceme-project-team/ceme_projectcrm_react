import CustomerDetailForm from "../customerForm/customerForm";
import React from "react";

function NewCustomer() {
    const initialFormState = {};

    return (
        <div>
            <h1>Add new Customer</h1>
            < br/>
            <CustomerDetailForm customer={initialFormState}/>
        </div>
    );
}
export default NewCustomer;