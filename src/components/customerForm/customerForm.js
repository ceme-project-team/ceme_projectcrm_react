import React, {useEffect, useState} from "react";
import "./customerForm.css"
import {useDispatch} from "react-redux";
import customerAdd from "../../redux/actions/CustomerAddAction";
import {useHistory} from "react-router-dom";
import {generateId} from "../../services/CustomerRestService";
import customerUpdate from "../../redux/actions/CustomerUpdateAction";

export default function CustomerDetailForm(props) {
    const dispatch = useDispatch();
    let findState;
    let newCustomer;

    if (props.customer.id && props.customer.id > 0) {
        findState = props.customer //find by id link
    }
    else if (props.customer.customerUpdate){
        findState = props.customer.customerUpdate;
    }
    else if (props.customer.history) {
        findState = props.customer.history.location.state //if you came from find all link
    } else {
        newCustomer = true;
        findState = {
            id: 0,
            prefix: "",
            firstName: "",
            middleName: "",
            lastName: "",
            suffix: "",
            gender: "",
            addressLine: "",
            city: "",
            state: "",
            zip: 0,
            phoneNumber: 0,
            emailAddress: "",
            dob: new Date().toISOString(),
            additionalInfo: ""
        } //brand new
    }

    const initialFormState = findState;
    const [customer, setCustomer] = useState(initialFormState);
    const handleInputChange = (event) => {
        const {name, value} = event.target;
        setCustomer({...customer, [name]: value});
    };

    useEffect(() => {
        if (!newCustomer) {
            return;
        }
            generateId()
            .then(res => res.data)
            .then(results => setCustomer({...customer, id: results}))
    }, []);


    const history = useHistory();
    const handleClickThrough = (props) => {
        history.push('/viewAll', {...props}) //do we want to go here or reload the page?
    }

    function validateForm(customer) {
        if(!customer.firstName.length>0 || customer.firstName.trim()===''){
            alert('Customer First Name cannot be empty!')
        } else if(!customer.lastName.length>0 || customer.lastName.trim()===''){
            alert('Customer Last Name cannot be empty!')
        } else if(!customer.addressLine.length>0 || customer.addressLine.trim()===''){
            alert('Customer Address Line cannot be empty!')
        } else if(!customer.city.length>0 || customer.city.trim()===''){
            alert('Customer City cannot be empty!')
        } else if(!customer.state.length>0 || customer.state.trim()===''){
            alert('Customer State cannot be empty!')
        } else if(!customer.zip>0|| Number.isNaN(parseInt(customer.zip))||customer.zip.toString().length < 5){
            alert('Customer ZIP cannot be empty and must contain more than 4 digits!')
        } else if(!customer.dob instanceof Date && !isNaN(customer.dob)){
            alert('Enter Valid Date!')}
        else {
            return true;
        }
    }

    return (
        <div>
            <form className="needs-validation" noValidate
                  onSubmit={(event) => {
                      event.preventDefault();
                      if(validateForm(customer)){
                          if(newCustomer) {
                              dispatch(customerAdd(customer));
                          }
                          else {
                              dispatch(customerUpdate(customer));
                          }
                      handleClickThrough(props)
                      }
                  }}>

                <div className="form-row">
                    <legend>Personal Information</legend>
                    <div className="col-sm-2 mb-3">
                        <label htmlFor="prefix">Prefix</label>
                        <input type="text" className="form-control" id="prefix" name="prefix" placeholder="Prefix"
                               value={customer.prefix}
                               onChange={handleInputChange}/>
                    </div>
                    <div className="col-sm-4 mb-3">
                        <label htmlFor="firstName">First name</label>
                        <input type="text" className="form-control" id="firstName" name="firstName" placeholder="First name"
                               value={customer.firstName} required
                               onChange={handleInputChange}/>
                    </div>
                    <div className="col-sm-6 mb-3">
                        <label htmlFor="middleName">Middle name(s)</label>
                        <input type="text" className="form-control" id="middleName" name="middleName" placeholder="Middle name"
                               value={customer.middleName}
                               onChange={handleInputChange}/>
                    </div>
                </div>
                <div className="form-row">
                    <div className="col-sm-8 mb-3">
                        <label htmlFor="lastName">Last name</label>
                        <input type="text" className="form-control" id="lastName" name="lastName" placeholder="Last name"
                               value={customer.lastName} required
                               onChange={handleInputChange}/>
                    </div>
                    <div className="col-sm-4 mb-3">
                        <label htmlFor="suffix">Suffix</label>
                        <input type="text" className="form-control" id="suffix" name="suffix" placeholder="Suffix"
                               value={customer.suffix}
                               onChange={handleInputChange}/>
                    </div>
                </div>
                <div className="form-row">
                    <div className="col-sm-6 mb-3">
                        <label htmlFor="gender">Gender</label>
                        <select type="text" className="form-control" id="gender" name="gender"
                                placeholder="Gender" value={customer.gender}
                                onChange={handleInputChange}>
                            <option value="Female">Female</option>
                            <option value="Male">Male</option>
                            <option value="Non-binary">Non-binary</option>
                            <option value="Other/Non-binary other">Other/Non-binary other</option>
                            <option value="Prefer not to say">Prefer not to say</option>
                        </select>
                    </div>
                    <div className="col-sm-6 mb-3">
                        <label htmlFor="dob">Date of Birth</label>
                        <input type="date" className="form-control" id="dob" name="dob"
                               placeholder="Enter DOB" value={customer.dob} required
                               onChange={handleInputChange}
                        />
                    </div>
                </div>
                <legend>Address Details</legend>
                <div className="form-row">
                    <div className="col-sm-12 mb-3">
                        <label htmlFor="addressLine">Address Line</label>
                        <input type="text" className="form-control" id="addressLine" name="addressLine" placeholder="Address Line"
                               value={customer.addressLine} required
                               onChange={handleInputChange}/>
                    </div>
                </div>
                <div className="form-row">
                    <div className="col-sm-6 mb-3">
                        <label htmlFor="city">City</label>
                        <input type="text" className="form-control" id="city" name="city" placeholder="City"
                               value={customer.city} required
                               onChange={handleInputChange}/>
                    </div>
                    <div className="col-sm-3 mb-3">
                        <label htmlFor="state">State</label>
                        <input type="text" className="form-control" id="state" name="state" placeholder="State"
                               value={customer.state} required
                               onChange={handleInputChange}/>
                    </div>
                    <div className="col-sm-3 mb-3">
                        <label htmlFor="zip">Zip</label>
                        <input type="number" className="form-control" id="zip" name="zip" placeholder="Zip" value={customer.zip}
                               required
                               onChange={handleInputChange}/>
                    </div>
                </div>
                <legend>Contact Details</legend>
                <div className="form-row">
                    <div className="col-sm-6 mb-3">
                        <label htmlFor="emailAddress">Email Address</label>
                        <input type="email" className="form-control" id="emailAddress" name="emailAddress"
                               placeholder="email@yourprovider.net"
                               value={customer.emailAddress}
                               onChange={handleInputChange}/>
                    </div>
                    <div className="col-sm-6 mb-3">
                        <label htmlFor="phoneNumber">Phone Number</label>
                        <input type="tel" className="form-control" id="phoneNumber" name="phoneNumber" placeholder="Phone Number"
                               value={customer.phoneNumber}
                               onChange={handleInputChange}/>
                    </div>
                </div>
                <legend>Additional Information</legend>
                <div>
                    <div className="col-sm-12 mb-3">
                        <textarea className="form-control" value={customer.additionalInfo} id="additionalInfo" name="additionalInfo"
                              onChange={handleInputChange}/>
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group">
                        <input type="submit" className="btn btn-primary" value="Save Customer"/>
                    </div>
                </div>
            </form>
        </div>

    )
}