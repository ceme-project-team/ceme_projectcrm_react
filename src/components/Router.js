import {Route, Switch} from "react-router-dom";
import Home from "./Home";
import React from "react";
import ViewAllCustomers from "./ViewAllCustomers"
import NewCustomer from "./newCustomer/newCustomer";
import UpdateCustomer from "./UpdateCustomer";
import Customer from "./Customer";
import NewInteraction from "./interactions/interactionForm/newInteraction";
import Status from "./Status";
import FindInteraction from "./interactions/interactionForm/FindInteraction";
import InteractionDetail from "./interactions/interactionForm/InteractionDetail";

function Router(){
    return (
        <div className="container mt-3">
            <Switch>
                <Route exact path={["/", "/home"]} component={Home}/>
                <Route exact path={["/", "/newcustomer"]} component={NewCustomer}/>
                <Route exact path="/viewAll" component={(viewAll) => <ViewAllCustomers {...viewAll}/>}/>
                <Route exact path="/customer" component={(customer) => <Customer {...customer}/>}/>
                <Route exact path="/update" component={(update) => <UpdateCustomer {...update}/>}/>
                <Route exact path="/newInteraction" component={NewInteraction}/>
                <Route exact path="/Status" component={Status}/>
                <Route exact path="/findInteraction" component={(props) => <FindInteraction {...props}/>}/>
                <Route exact path="/interactionDetail" component={(interactionDetail) => <InteractionDetail {...interactionDetail}/>}/>=

            {/*/!*    <Route exact path="/" component={}/>*!/*/}
            {/*    <Route exact path="/customer/:id" component={}/>*/}
            </Switch>
        </div>
    )
}

export default Router;