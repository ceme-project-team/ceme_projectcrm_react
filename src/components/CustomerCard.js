import {picture} from "../scripts/common";
import {Link, withRouter} from "react-router-dom";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {confirmAlert} from "react-confirm-alert";
import customerDelete from "../redux/actions/CustomerDeleteAction"

function CustomerCard(props) {

    const dispatch = useDispatch();
    const customers = useSelector(state => state.CustomerListReducer.customers)

    const handleButtonClick = (customer, type, e) => {
        console.log("testing button click");
        let lowercaseType = type.toString().toLowerCase();

        if (customer.id) {
            console.log("testing : " + lowercaseType + " " + customer.id);

            confirmAlert({
                title: "Confirm to " + lowercaseType + " customer",
                message: "Do you really want to " + lowercaseType + " the customer with ID: " + customer.id + "?",
                buttons: [
                    {
                        label: "Yes",
                        onClick: () => (
                            conditions(lowercaseType, customer))
                    },
                    {
                        label: "No",
                        onClick: () => false
                    }
                ]
            });

            const conditions = (lowercaseType, customer) => {
                switch (lowercaseType){
                    case "delete":
                        doDelete(customer)
                        break;
                    case "update":
                        doUpdate(customer)
                        break;
                    case "addinteraction":
                        doAddInteraction(customer.id)
                        break;
                    default: break;
                }
            }
            const doDelete = (customer) => {
                dispatch(customerDelete(customer));
                window.location.reload(true);
            }

            const doUpdate = (customer) => {
                props.history.push("/update",{
                    customerUpdate: customer
                    })
                };

            const doAddInteraction = (clientId) => {
                props.history.push("/newInteraction",{
                    interactionAdd: clientId
                })
            };
        }
    }

    return (
        <div className={"container"}> {(customers) && (customers.map((item, index) => (
                <div className="card mb-3" key={index}>
                    <img className="card-img" src={picture(item.gender)} alt="Card image"/>
                    <div className="card-body">
                        <h5 className="card-title">Customer name: {item.prefix} {item.lastName}</h5>
                        <p className="card-text"><u><b></b></u><br/>
                            Date of birth: {item.dob} <br/>
                            Email Address: {item.emailAddress}<br/>
                            Phone Number: {item.phoneNumber} <br/>
                        </p>
                        <Link to={{
                            pathname: "/customer",
                            state: {id: item.id}}}>More Details</Link>
                        <br/>
                        <br/>
                        <button className="btn btn-primary"
                                onClick={(e) => handleButtonClick(item, "DELETE", e)}>Delete
                        </button>

                        <button className="btn btn-primary"
                                onClick={(e) => handleButtonClick(item, "UPDATE", e)}>Update
                        </button>
                        <button className="btn btn-primary"
                                onClick={(e) => handleButtonClick(item, "ADDINTERACTION", e)}>Add new Interaction
                        </button>
                    </div>
                </div>
            )))}
        </div>
    )
}

export default withRouter(CustomerCard);