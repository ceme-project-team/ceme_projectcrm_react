import React, {useState} from "react";
import {useDispatch} from "react-redux";
import interactionAdd from "../../../redux/actions/InteractionAddAction";
import {useHistory} from "react-router-dom";

export default function InteractionForm(props) {
    const dispatch = useDispatch();
    let findState

    if (props.interaction) {
        //for update
        findState = { timeStamp: new Date().toISOString(),
            action:"",
            clientId: props.interaction.interactionAdd,
            notes:"",
            disabled: true}

    } else {
        //for add new
        findState = {
            timeStamp: new Date().toISOString(),
            action:"",
            clientId:0,
            notes:"",
            disabled: false
        }
    }

    const initialFormState = findState;
    const [interaction, setInteraction] = useState(initialFormState);

    const handleInputChange = (event) => {
        const {name, value} = event.target;
        setInteraction({...interaction, [name]: value});
    };

    const history = useHistory();
    const handleClickThrough = () => {
        history.push('/home') //do we want to go here or reload the page?
    }
    function validateForm(interaction) {
        if(!interaction.action.length > 0 || interaction.action.trim()===''){
            alert('Interaction field cannot be empty')
        }
        else {
            return true;
        }
    }
    return(
        <div>
            <form className="needs-validation" noValidate
                  onSubmit={(event) => {
                      event.preventDefault();
                      if(validateForm(interaction)){
                          dispatch(interactionAdd(interaction));
                          handleClickThrough()
                      }
                  }}>
        <div className="form-row">
            <legend>Interaction Information</legend>
            <div className="col-sm-8 mb-3">
                <label htmlFor="clientId">Client ID</label>
                <input type="text" className="form-control" id="clientId" name="clientId" placeholder="Client ID"
                       value={interaction.clientId}
                       onChange={handleInputChange} disabled={interaction.disabled}/>
            </div>
        </div>
                <div className="form-row">
                    <div className="col-sm-8 mb-3">
                        <label htmlFor="action">Action</label>
                        <input type="text" className="form-control" id="action" name="action" placeholder="Action"
                               value={interaction.action}
                               onChange={handleInputChange}/>
                    </div>
                </div>
                <div className="form-row">
                    <div className="col-sm-8 mb-3">
                        <label htmlFor="notes">Notes</label>
                        <textarea type="text" className="form-control" id="notes" name="notes" placeholder="Notes"
                               value={interaction.notes}
                               onChange={handleInputChange}/>
                    </div>
                </div>
                <div className="form-row">
                    <div className="col-sm-8 mb-3">
                        <label htmlFor="timestamp">Date</label>
                        <input type="text" className="form-control" id="timeStamp" name="timeStamp" placeholder="Action"
                               value={interaction.timeStamp}
                               onChange={handleInputChange} disabled/>
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group">
                        <input type="submit" className="btn btn-primary" value="Save Interaction"/>
                    </div>
                </div>
            </form>
        </div>
    )
}