import React, {useEffect} from "react";
import {useSelector} from "react-redux";

 function Details(props) {

     const interactionDetail = useSelector(state => state.InteractionFindByIdReducer.interactions);

     return (
         <div className={"container"}> {(interactionDetail)&&interactionDetail.map((item, index) => (
             <div className="card mb-3" key={index}>
                 <div className="card-body">
                     <h5 className="card-title">Interaction Details</h5>
                     <p className="card-text"><u><b></b></u><br/>
                         Client ID: {item.clientId} <br/>
                         Activity Timestamp: {item.timeStamp}<br/>
                         Action: {item.action} <br/>
                         Notes: {item.notes} <br/>
                     </p>
                 </div>
             </div>

             ))}
         </div>
     )
 }
export default Details;