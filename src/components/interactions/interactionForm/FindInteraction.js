import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import getInteractionsByClientId from "../../../redux/actions/InteractionFindByAction";
import {withRouter} from "react-router-dom"

function InteractionClientId(props) {
    const dispatch = useDispatch();

    const initialFormState = {
        custId: 0,
        submitted: false
    };

    const [searchType, setSearchType] = useState(initialFormState);

    const handleInputChange = (event) => {

        setSearchType({
            custId: event.target.value
       })

    };

    const doView = () => {
        console.log();
        props.history.push("/interactionDetail")
        }


    return (
        <div className="col-md-6">
            <h4>Find by Client ID</h4>
            <p>Search for an interaction by the Client ID</p>
            <form className="form-inline"
                  onSubmit={(event) => {

                      event.preventDefault();
                      dispatch(getInteractionsByClientId(searchType.custId));
                      doView();
                  }}
            >
                <div className="form-group mx-sm-3 mb-2">
                    <label htmlFor="type" className="sr-only">Find By Client ID</label>
                    <input type="number" className="form-control" id="clientId" name="clientId" placeholder="Find by Client ID"
                           value={searchType.clientId} onChange={handleInputChange}/>
                </div>
                <button type="submit" className="btn btn-primary mb-2">Search</button>
            </form>
        </div>
    )
}

export default withRouter(InteractionClientId);