import InteractionForm from "./interactionForm";
import React from "react";

function NewInteraction(props) {
      return (
        <div>
            <h4>Add new Customer Interaction</h4>
            < br/>
            <InteractionForm interaction={props.location.state}/>
        </div>
    );
}
export default NewInteraction;