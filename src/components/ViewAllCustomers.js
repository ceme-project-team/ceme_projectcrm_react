import React, {useEffect} from 'react';
import '../styles/viewallcustomerscss.css'
import {useDispatch, useSelector} from "react-redux";
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import customerList from "../redux/actions/CustomerListAction";
import CustomerCard from "./CustomerCard";


 function ViewAllCustomers(props) {
    const dispatch = useDispatch();

    useEffect(() => {
         dispatch(customerList())},
         []);

        return (<div>
            <h1>View All Customers</h1>
            <CustomerCard {...props}/>
        </div>)
}
export default ViewAllCustomers;