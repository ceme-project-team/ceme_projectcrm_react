import React, {useEffect} from 'react';
import '../styles/viewallcustomerscss.css'
import {useDispatch, useSelector} from "react-redux";
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import getCustomerById from "../redux/actions/customerFindByAction"
import {picture} from "../scripts/common";
import getInteractionsByClientId from "../redux/actions/InteractionFindByAction";

function Customer(props) {
    const dispatch = useDispatch();

     useEffect(() => {
            dispatch(getCustomerById(props.location.state.id))},
          []);

    useEffect(() => {
            dispatch(getInteractionsByClientId(props.location.state.id))},
        []);

    const customer = useSelector(state => state.CustomerFindByIdReducer.customer)
    const customerInteractions = useSelector(state => state.InteractionFindByIdReducer.interactions);


    return (<div>
             <h1>View Customers by ID</h1>
                {(customer.id) ? (
                    <div className="card mb-3">
                        <img className="card-img" src={picture(customer.gender)} alt="Card image" height={"30%"} width={"50%"}/>
                        <div className="card-body">
                            <h5 className="card-title">Customer name: {customer.prefix} {customer.lastName}</h5>
                            <p className="card-text"><u><b>Customer Details</b></u><br/>
                                ID: {customer.id}<br/>
                                First Name: {customer.firstName}<br/>
                                Middle Name: {customer.middleName}<br/>
                                Suffix: {customer.suffix}<br/>
                                Gender: {customer.gender}<br/>
                                Address Line: {customer.addressLine}<br/>
                                City: {customer.city}<br/>
                                State: {customer.state}<br/>
                                Zip:{customer.zip}<br/>
                                Phone Number: {customer.phoneNumber} <br/>
                                Email Address: {customer.emailAddress}<br/>
                                Date of birth: {customer.dob} <br/>
                                Additional Info: {customer.additionalInfo}<br/>
                            </p>
                        </div>
                    </div>
                ):(<div>Nothing to see here</div>)}
        {
            <div className="list-group">
                <legend>Customer Interactions</legend>
                <table className="table  table-hover">
                    <tbody>
                    {customerInteractions && customerInteractions
                      //  .sort((a, b) => new Date(b.timeStamp) - new Date(a.timeStamp))
                        .map((interactions, index) => {
                        return [
                            <tr key={index}>
                                <td>{interactions.timeStamp}</td>
                                <td>{interactions.action}</td>
                                <td>{interactions.notes}</td>
                            </tr>
                        ];
                    })}
                    </tbody>
                </table>
            </div>
        }
            </div>);
}
export default Customer;