import React, {Component} from 'react';
import {getStatus} from "../services/CustomerRestService";
import restAPI from "../images/rest_api.png"

export default class Status extends Component {
    constructor(props) {
        super(props);

        this.state = {
            status: "UNKNOWN"
        }
    }

    retrieveApiStatus() {
        getStatus()
            .then(response => {
                console.log(response.data)
                this.setState({
                    status: response.data
                })
            })
            .catch(e => {
                console.log(e)
            })
    }

    componentDidMount() {
        this.retrieveApiStatus()
    }

    render() {
        return (
          <div className="container ">
                <h1 className="masthead-heading text-uppercase"> Customer Rest API Status</h1>
                <h4 className="masthead-subheading">{this.state.status}</h4>
              <img className="img-fluid" src={restAPI}  alt="Rest API"/>
            </div>
        )
    }
}