import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import {Link} from "react-router-dom";
import navBarPhoto from "../images/allstate_logo.jpg"

function Navigation() {
    return (
        <header>
            <nav className="navbar static-top navbar navbar-expand-lg navbar-light bg-light">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <a className="nav-link" href="/home">WhataBoutYe<span className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/Status">Status</a>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Customers
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a className="dropdown-item" tag={Link} href="/viewAll">
                                    View All
                                </a>
                                <a tag={Link} href={"/newCustomer"} className="dropdown-item">
                                    Add New Customer
                                </a>
                            </div>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Interactions
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a tag={Link} href={"/newInteraction"} className="dropdown-item">
                                    New Interaction
                                </a>
                                <a tag={Link} href={"/findInteraction"} className="dropdown-item">
                                    Find Interaction
                                </a>
                            </div>
                        </li>
                    </ul>
                    <span className="navbar-text">
                        <img src={navBarPhoto} alt={"Allstate"} height={'50px'}/>
                    </span>
                </div>
            </nav>
        </header>
    )
}

export default Navigation;